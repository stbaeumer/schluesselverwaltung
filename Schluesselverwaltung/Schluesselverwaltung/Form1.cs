﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Schluesselverwaltung
{
    public partial class Schlüsselverwaltung : Form
    {
        internal Persons Persons { get; private set; }
        internal Keys Keys { get; private set; }
        public bool x { get; private set; }
        private Font verdana10Font;
        private StreamReader reader;
        public Schlüsselverwaltung()
        {
            InitializeComponent();
        }

        private void Schlüsselverwaltung_Load(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Lehrerbilder = @"\\fs01\SoftwarehausHeider\Atlantis\Dokumente\jpg";
                Properties.Settings.Default.ConnectionString = @"Dsn=Atlantis9;uid=DBA";
                Properties.Settings.Default.Save();

                int aktSj = DateTime.Now.Year;
                string aktSjAtlantis = aktSj.ToString() + "/" + (aktSj + 1 - 2000);

                Keys = new Keys();
                Persons = new Persons(aktSjAtlantis);

                comboBoxPersonal.DataSource = (from p in Persons select p.Nachname + ", " + p.Vorname + " (" + p.Kürzel + ")").ToList();                
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        private void CheckedListBoxKeys_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (x != true)
            {
                if (checkedListBoxKeys.GetItemCheckState(checkedListBoxKeys.SelectedIndex) == CheckState.Checked)
                {
                    Key key = Keys[checkedListBoxKeys.SelectedIndex];
                    key.AusgabeAm = DateTime.Now;
                    Persons[comboBoxPersonal.SelectedIndex].KeyList.Add(key);

                }
                else
                {
                    int i = Persons[comboBoxPersonal.SelectedIndex].KeyList.IndexOf((from k in Persons[comboBoxPersonal.SelectedIndex].KeyList where k.Id == Keys[checkedListBoxKeys.SelectedIndex].Id select k).FirstOrDefault());

                    Persons[comboBoxPersonal.SelectedIndex].KeyList.RemoveAt(0);
                }
            }
            x = false;            
        }
        
        private void comboBoxPersonal_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkBoxAktiv.Checked = Persons[comboBoxPersonal.SelectedIndex].Aktiv == "J" ? true : false;
            tbxNachname.Text = Persons[comboBoxPersonal.SelectedIndex].Nachname;
            tbxVorname.Text = Persons[comboBoxPersonal.SelectedIndex].Vorname;
            tbxId.Text = Persons[comboBoxPersonal.SelectedIndex].IdAtlantis.ToString();
            tbxEintrittsdatum.Text = Persons[comboBoxPersonal.SelectedIndex].Eintrittsdatum.ToShortDateString();
            tbxAustrittsdatum.Text = Persons[comboBoxPersonal.SelectedIndex].Austrittsdatum.ToShortDateString();
            tbxKürzel.Text = Persons[comboBoxPersonal.SelectedIndex].Kürzel;
            pbxLehrer.Image = Image.FromFile(Persons[comboBoxPersonal.SelectedIndex].Bilddatei);
            BelegeAnzeigen();
            listBoxQuittungen.DataSource = Persons[comboBoxPersonal.SelectedIndex].Belege;

            checkedListBoxKeys.DataSource = (from k in Keys select k.Beschreibung).ToList();
            foreach (var key in Keys)
            {
                // Wenn der angezeigte Key im Besitz der Person ist, wird er angehakt.
                if ((from k in Persons[comboBoxPersonal.SelectedIndex].KeyList where k.Id == key.Id select k).Any())
                {
                    checkedListBoxKeys.SetItemCheckState(Keys.IndexOf(key), CheckState.Checked);
                }
                else
                {
                    checkedListBoxKeys.SetItemCheckState(Keys.IndexOf(key), CheckState.Unchecked);
                }
            }
        }

        private void comboBoxPersonal_SelectionChangeCommitted(object sender, EventArgs e)
        {
            x = true;
        }

        private void btnBelegDrucken_Click(object sender, EventArgs e)
        {

            string fileName = Persons[comboBoxPersonal.SelectedIndex].Kürzel + "-" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

            try
            {
                // Check if file already exists. If yes, delete it. 
                if (File.Exists(Path.Combine(@"c:\\users\bm\Desktop\", fileName)))
                {
                    File.Delete(Path.Combine(@"c:\\users\bm\Desktop\", fileName));
                }

                // Create a new file 
                using (var dest = File.AppendText(Path.Combine(@"c:\\users\bm\Desktop\", fileName))) 
                {
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("E M P F A N G S B E S T Ä T I G U N G");
                    dest.WriteLine("=====================================");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("Folgende Schlüssel wurden mir zum Dienstgebrauch ausgehändigt:");

                    dest.WriteLine("");
                    dest.WriteLine("------------------------------------------------------------------");
                    dest.WriteLine(" Id".PadRight(7) + "Beschreibung".PadRight(25) + "Ausgabe am");
                    dest.WriteLine("------------------------------------------------------------------");
                    foreach (var key in Persons[comboBoxPersonal.SelectedIndex].KeyList)
                    {
                        dest.WriteLine(key.Id.ToString().PadLeft(3) + "    " +  key.Beschreibung.PadRight(24) + " " + key.AusgabeAm.ToShortDateString());
                    }
                    dest.WriteLine("------------------------------------------------------------------");
                    dest.WriteLine("Anzahl: " + Persons[comboBoxPersonal.SelectedIndex].KeyList.Count);
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("Das bestätige ich mit meiner Unterschrift.");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("Borken, " + DateTime.Now.ToShortDateString());
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("");
                    dest.WriteLine("__________________________________");
                    dest.WriteLine(Persons[comboBoxPersonal.SelectedIndex].Vorname + " " + Persons[comboBoxPersonal.SelectedIndex].Nachname);
                }

                SendToPrinter(Path.Combine(@"c:\\users\bm\Desktop\", fileName));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            BelegeAnzeigen();
        }

        private void BelegeAnzeigen()
        {            
            listBoxQuittungen.DataSource = (from b in Persons[comboBoxPersonal.SelectedIndex].Belege select b.Zeitstempel + "  ( " + b.Keys.Count + " Schlüssel )").ToList();
        }

        private void SendToPrinter(string path)
        {   
            string filename = path;
            //Create a StreamReader object
            reader = new StreamReader(filename);
            //Create a Verdana font with size 10
            verdana10Font = new Font("Courier New", 12);
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //Add PrintPage event handler
            pd.PrintPage += new PrintPageEventHandler(this.PrintTextFileHandler);
            //Call Print Method
            pd.Print();
            //Close the reader
            if (reader != null)
                reader.Close();
        }

        private void PrintTextFileHandler(object sender, PrintPageEventArgs ppeArgs)
        {
            //Get the Graphics object
            Graphics g = ppeArgs.Graphics;
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            //Read margins from PrintPageEventArgs
            float leftMargin = ppeArgs.MarginBounds.Left;
            float topMargin = ppeArgs.MarginBounds.Top;
            string line = null;
            //Calculate the lines per page on the basis of the height of the page and the height of the font
            linesPerPage = ppeArgs.MarginBounds.Height /
            verdana10Font.GetHeight(g);
            //Now read lines one by one, using StreamReader
            while (count < linesPerPage &&
            ((line = reader.ReadLine()) != null))
            {
                //Calculate the starting position
                yPos = topMargin + (count *
                verdana10Font.GetHeight(g));
                //Draw text
                g.DrawString(line, verdana10Font, Brushes.Black,
                leftMargin, yPos, new StringFormat());
                //Move to next line
                count++;
            }
            //If PrintPageEventArgs has more pages to print
            if (line != null)
            {
                ppeArgs.HasMorePages = true;
            }
            else
            {
                ppeArgs.HasMorePages = false;
            }
            Persons[comboBoxPersonal.SelectedIndex].Belege.Add(new Beleg(DateTime.Now, Persons[comboBoxPersonal.SelectedIndex].KeyList));
        }
    }
}
