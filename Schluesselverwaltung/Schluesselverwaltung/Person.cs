﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schluesselverwaltung
{
    class Person
    {
        public string Kürzel { get; internal set; }
        public int IdAtlantis { get; internal set; }
        public string Nachname { get; internal set; }
        public string Vorname { get; internal set; }
        public string Aktiv { get; internal set; }
        public DateTime Eintrittsdatum { get; internal set; }
        public DateTime Austrittsdatum { get; internal set; }
        public string Bilddatei { get; internal set; }
        public List<Key> KeyList { get; set; }
        public List<Beleg> Belege { get; set; }

        public Person()
        {
            KeyList = new List<Key>();
            Belege = new List<Beleg>();
        }
    }
}
