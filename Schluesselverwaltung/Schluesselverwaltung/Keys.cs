﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schluesselverwaltung
{
    class Keys:List<Key>
    {
        public Keys()
        {
            this.Add(new Key(1, "Beschreibung1"));
            this.Add(new Key(2, "Beschreibung2"));
            this.Add(new Key(3, "Beschreibung3"));
            this.Add(new Key(4, "Beschreibung4"));
            this.Add(new Key(5, "Beschreibung5"));
            this.Add(new Key(6, "Beschreibung6"));
        }
    }
}
