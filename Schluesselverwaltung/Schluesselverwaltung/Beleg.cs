﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schluesselverwaltung
{
    class Beleg
    {
        private DateTime now;

        public Beleg(DateTime now, List<Key> keys)
        {
            Zeitstempel = now;
            Keys = keys;
        }

        public DateTime Zeitstempel { get; set; }
        public List<Key> Keys { get; set; }
    }
}
