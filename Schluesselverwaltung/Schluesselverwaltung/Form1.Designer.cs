﻿namespace Schluesselverwaltung
{
    partial class Schlüsselverwaltung
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbxLehrer = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxKürzel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBoxAktiv = new System.Windows.Forms.CheckBox();
            this.tbxAustrittsdatum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxEintrittsdatum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxVorname = new System.Windows.Forms.TextBox();
            this.comboBoxPersonal = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNachname = new System.Windows.Forms.TextBox();
            this.tbxId = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.listBoxQuittungen = new System.Windows.Forms.ListBox();
            this.btnBelegDrucken = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkedListBoxKeys = new System.Windows.Forms.CheckedListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLehrer)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbxLehrer
            // 
            this.pbxLehrer.Location = new System.Drawing.Point(10, 19);
            this.pbxLehrer.Name = "pbxLehrer";
            this.pbxLehrer.Size = new System.Drawing.Size(160, 169);
            this.pbxLehrer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxLehrer.TabIndex = 8;
            this.pbxLehrer.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbxKürzel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.checkBoxAktiv);
            this.groupBox1.Controls.Add(this.tbxAustrittsdatum);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbxEintrittsdatum);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbxVorname);
            this.groupBox1.Controls.Add(this.comboBoxPersonal);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbxNachname);
            this.groupBox1.Controls.Add(this.tbxId);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(208, 435);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personal";
            // 
            // tbxKürzel
            // 
            this.tbxKürzel.Enabled = false;
            this.tbxKürzel.Location = new System.Drawing.Point(13, 269);
            this.tbxKürzel.Name = "tbxKürzel";
            this.tbxKürzel.Size = new System.Drawing.Size(49, 20);
            this.tbxKürzel.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(155, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 9);
            this.label6.TabIndex = 23;
            this.label6.Text = "Id";
            // 
            // checkBoxAktiv
            // 
            this.checkBoxAktiv.AutoSize = true;
            this.checkBoxAktiv.Enabled = false;
            this.checkBoxAktiv.Location = new System.Drawing.Point(81, 272);
            this.checkBoxAktiv.Name = "checkBoxAktiv";
            this.checkBoxAktiv.Size = new System.Drawing.Size(49, 17);
            this.checkBoxAktiv.TabIndex = 22;
            this.checkBoxAktiv.Text = "aktiv";
            this.checkBoxAktiv.UseVisualStyleBackColor = true;
            // 
            // tbxAustrittsdatum
            // 
            this.tbxAustrittsdatum.Enabled = false;
            this.tbxAustrittsdatum.Location = new System.Drawing.Point(13, 405);
            this.tbxAustrittsdatum.Name = "tbxAustrittsdatum";
            this.tbxAustrittsdatum.Size = new System.Drawing.Size(181, 20);
            this.tbxAustrittsdatum.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 393);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 9);
            this.label5.TabIndex = 21;
            this.label5.Text = "Austrittsdatum";
            // 
            // tbxEintrittsdatum
            // 
            this.tbxEintrittsdatum.Enabled = false;
            this.tbxEintrittsdatum.Location = new System.Drawing.Point(13, 370);
            this.tbxEintrittsdatum.Name = "tbxEintrittsdatum";
            this.tbxEintrittsdatum.Size = new System.Drawing.Size(181, 20);
            this.tbxEintrittsdatum.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 358);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 9);
            this.label4.TabIndex = 19;
            this.label4.Text = "Eintrittsdatum";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 325);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 9);
            this.label3.TabIndex = 18;
            this.label3.Text = "Vorname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 9);
            this.label2.TabIndex = 15;
            this.label2.Text = "Nachname";
            // 
            // tbxVorname
            // 
            this.tbxVorname.Enabled = false;
            this.tbxVorname.Location = new System.Drawing.Point(13, 335);
            this.tbxVorname.Name = "tbxVorname";
            this.tbxVorname.Size = new System.Drawing.Size(181, 20);
            this.tbxVorname.TabIndex = 18;
            // 
            // comboBoxPersonal
            // 
            this.comboBoxPersonal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxPersonal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxPersonal.FormattingEnabled = true;
            this.comboBoxPersonal.Location = new System.Drawing.Point(13, 19);
            this.comboBoxPersonal.Name = "comboBoxPersonal";
            this.comboBoxPersonal.Size = new System.Drawing.Size(181, 21);
            this.comboBoxPersonal.TabIndex = 0;
            this.comboBoxPersonal.SelectedIndexChanged += new System.EventHandler(this.comboBoxPersonal_SelectedIndexChanged);
            this.comboBoxPersonal.SelectionChangeCommitted += new System.EventHandler(this.comboBoxPersonal_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 258);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 9);
            this.label1.TabIndex = 14;
            this.label1.Text = "Kürzel";
            // 
            // tbxNachname
            // 
            this.tbxNachname.Enabled = false;
            this.tbxNachname.Location = new System.Drawing.Point(13, 302);
            this.tbxNachname.Name = "tbxNachname";
            this.tbxNachname.Size = new System.Drawing.Size(181, 20);
            this.tbxNachname.TabIndex = 16;
            // 
            // tbxId
            // 
            this.tbxId.Enabled = false;
            this.tbxId.Location = new System.Drawing.Point(151, 270);
            this.tbxId.Name = "tbxId";
            this.tbxId.Size = new System.Drawing.Size(43, 20);
            this.tbxId.TabIndex = 17;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pbxLehrer);
            this.groupBox3.Location = new System.Drawing.Point(13, 46);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(181, 207);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Location = new System.Drawing.Point(226, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 435);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Auswahl";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.listBoxQuittungen);
            this.groupBox5.Controls.Add(this.btnBelegDrucken);
            this.groupBox5.Location = new System.Drawing.Point(17, 273);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(232, 149);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Empfangsquittungen";
            // 
            // listBoxQuittungen
            // 
            this.listBoxQuittungen.FormattingEnabled = true;
            this.listBoxQuittungen.Location = new System.Drawing.Point(7, 20);
            this.listBoxQuittungen.Name = "listBoxQuittungen";
            this.listBoxQuittungen.Size = new System.Drawing.Size(219, 95);
            this.listBoxQuittungen.TabIndex = 1;
            // 
            // btnBelegDrucken
            // 
            this.btnBelegDrucken.Location = new System.Drawing.Point(6, 120);
            this.btnBelegDrucken.Name = "btnBelegDrucken";
            this.btnBelegDrucken.Size = new System.Drawing.Size(220, 23);
            this.btnBelegDrucken.TabIndex = 0;
            this.btnBelegDrucken.Text = "Beleg drucken";
            this.btnBelegDrucken.UseVisualStyleBackColor = true;
            this.btnBelegDrucken.Click += new System.EventHandler(this.btnBelegDrucken_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkedListBoxKeys);
            this.groupBox4.Location = new System.Drawing.Point(17, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(232, 248);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Empfangene Schlüssel";
            // 
            // checkedListBoxKeys
            // 
            this.checkedListBoxKeys.BackColor = System.Drawing.SystemColors.Control;
            this.checkedListBoxKeys.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBoxKeys.CheckOnClick = true;
            this.checkedListBoxKeys.FormattingEnabled = true;
            this.checkedListBoxKeys.Items.AddRange(new object[] {
            "Innenschlüssel 1",
            "Innenschlüssel 2",
            "Innenschlüssel 3"});
            this.checkedListBoxKeys.Location = new System.Drawing.Point(33, 43);
            this.checkedListBoxKeys.Name = "checkedListBoxKeys";
            this.checkedListBoxKeys.Size = new System.Drawing.Size(120, 90);
            this.checkedListBoxKeys.TabIndex = 0;
            this.checkedListBoxKeys.SelectedIndexChanged += new System.EventHandler(this.CheckedListBoxKeys_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 451);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(510, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "BM 2017";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(52, 17);
            this.toolStripStatusLabel1.Text = "BM 2017";
            // 
            // Schlüsselverwaltung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 473);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Schlüsselverwaltung";
            this.Text = "Schlüsselverwaltung";
            this.Load += new System.EventHandler(this.Schlüsselverwaltung_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxLehrer)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbxLehrer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnBelegDrucken;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListBoxKeys;
        private System.Windows.Forms.ComboBox comboBoxPersonal;
        private System.Windows.Forms.TextBox tbxNachname;
        private System.Windows.Forms.CheckBox checkBoxAktiv;
        private System.Windows.Forms.TextBox tbxAustrittsdatum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxEintrittsdatum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxVorname;
        private System.Windows.Forms.TextBox tbxId;
        private System.Windows.Forms.TextBox tbxKürzel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listBoxQuittungen;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

