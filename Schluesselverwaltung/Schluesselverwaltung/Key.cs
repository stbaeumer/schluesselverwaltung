﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schluesselverwaltung
{
    class Key
    {
        public Key(int id, string beschreibung)
        {
            this.Id = id;
            this.Beschreibung = beschreibung;
        }

        public int Id { get; set; }
        public string Beschreibung { get; set; }
        public DateTime AusgabeAm { get; set; }
    }
}
