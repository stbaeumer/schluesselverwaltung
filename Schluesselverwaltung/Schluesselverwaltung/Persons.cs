﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schluesselverwaltung
{
    class Persons:List<Person>
    {
        public Persons(string aktSjAtlantis)
        {
            try
            {
                var bildDateien = Directory.GetFiles(Properties.Settings.Default.Lehrerbilder, "*.jpg", SearchOption.AllDirectories).ToList();

                using (OdbcConnection connection = new OdbcConnection(Properties.Settings.Default.ConnectionString))
                {
                    DataSet dataSet = new DataSet();

                    OdbcDataAdapter lehrerAdapter = new OdbcDataAdapter(@"SELECT DBA.lehrer.le_id AS IdAtlantis,
DBA.lehrer.le_kuerzel AS Kürzel,
DBA.lehrer.name_1 AS Nachname,
DBA.lehrer.name_2 AS Vorname,
DBA.lehrer.aktiv_jn AS Aktiv,
DBA.lehrer.dat_austritt AS Austrittsdatum,
DBA.lehr_sc.dat_eintritt AS Eintrittsdatum
FROM DBA.lehr_sc JOIN DBA.lehrer ON DBA.lehr_sc.le_id = DBA.lehrer.le_id
WHERE vorgang_schuljahr = '" + aktSjAtlantis + @"'ORDER BY DBA.lehr_sc.ls_kuerzel ASC", connection);


                    try
                    {
                        connection.Open();
                        lehrerAdapter.Fill(dataSet, "DBA.lehrer");

                        foreach (DataRow theRow in dataSet.Tables["DBA.lehrer"].Rows)
                        {
                            var idAtlantis = theRow["IdAtlantis"] == null ? -99 : Convert.ToInt32(theRow["IdAtlantis"]);
                            var bildDatei = (from b in bildDateien where b.Contains("LE") where b.Contains(idAtlantis.ToString()) select b).FirstOrDefault();
                            if(bildDatei == null)
                                bildDatei = (from b in bildDateien where b.Contains("kein") select b).FirstOrDefault();
                            Person person = new Person()
                            {
                                Kürzel = theRow["Kürzel"] == null ? "" : theRow["Kürzel"].ToString(),
                                IdAtlantis = idAtlantis,
                                Nachname = theRow["Nachname"] == null ? "" : theRow["Nachname"].ToString(),
                                Vorname = theRow["Vorname"] == null ? "" : theRow["Vorname"].ToString(),
                                Aktiv = theRow["Aktiv"] == null ? "" : theRow["Aktiv"].ToString(),
                                Eintrittsdatum = theRow["Eintrittsdatum"].ToString().Length < 3 ? new DateTime() : DateTime.ParseExact(theRow["Eintrittsdatum"].ToString(), "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                                Austrittsdatum = theRow["Austrittsdatum"].ToString().Length < 3 ? new DateTime() : DateTime.ParseExact(theRow["Austrittsdatum"].ToString(), "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                                Bilddatei = bildDatei
                            };
                            this.Add(person);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }            
        }
    }
}
